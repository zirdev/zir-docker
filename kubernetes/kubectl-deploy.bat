kubectl create -f db-env-configmap.yaml
kubectl create -f graphql-env-configmap.yaml
kubectl create -f ..\secret\dev\secret.kubernetes.postgresql.yaml
kubectl create -f ..\secret\dev\secret.kubernetes.graphql.yaml
kubectl create -f db-persistentvolumeclaim.yaml
kubectl create -f 01-db-service.yaml
kubectl create -f 01-graphql-service.yaml
kubectl create -f db-deployment.yaml
kubectl create -f graphql-deployment.yaml
