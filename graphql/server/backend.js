const http = require("http");
const url = require("url");
const path = require("path");
const fs = require("fs");
const port = process.env.PORT || 8888;

http
  .createServer(function(request, response) {
    var uri = url.parse(request.url).pathname,
      // filename = path.join(process.cwd(), uri);
      filename = path.join(__dirname, "", uri);

    var contentTypesByExtension = {
      ".html": "text/html",
      ".css": "text/css",
      ".js": "text/javascript"
    };

    fs.exists(filename, function(exists) {
      if (!exists) {
        // response.writeHead(404, { "Content-Type": "text/plain" });
        // response.write("404 Not Found\n");
        response.redirect('index.html');
        response.end();
        return;
      }

      if (fs.statSync(filename).isDirectory()) filename += "/index.html";

      fs.readFile(filename, "binary", function(err, file) {
        if (err) {
          response.writeHead(500, { "Content-Type": "text/plain" });
          response.write(err + "\n");
          response.end();
          return;
        }

        var headers = {};
        var contentType = contentTypesByExtension[path.extname(filename)];
        if (contentType) headers["Content-Type"] = contentType;
        response.writeHead(200, headers);
        response.write(file, "binary");
        response.end();
      });
    });
  })
  .listen(parseInt(port, 10));

// fs.chown(
//   path.join(__dirname, "../../_production/"),
//   777,
//   console.log("permissions done")
// );

console.log("Static file server running at\n  => http://localhost:" + port + "/\nCTRL + C to shutdown");
