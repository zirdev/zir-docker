const path = require("path");
process.chdir(__dirname);
require("dotenv").config({
  path: path.normalize(path.join(__dirname, "../../../../")),
});
const { createServer } = require("http");
const express = require("express");
const {
  postgraphile,
  enhanceHttpServerWithSubscriptions,
  makePluginHook,
} = require("postgraphile");
const PgSimplifyInflectorPlugin = require("@graphile-contrib/pg-simplify-inflector");
const ConnectionFilterPlugin = require("postgraphile-plugin-connection-filter");
const PgOrderByRelatedPlugin = require("@graphile-contrib/pg-order-by-related");
const PgManyToManyPlugin = require("@graphile-contrib/pg-many-to-many");
// const PgOrderByMultiColumnIndexPlugin = require("@graphile-contrib/pg-order-by-multi-column-index");
const { default: PgPubsub } = require("@graphile/pg-pubsub");

const app = express();
const rawHTTPServer = createServer(app);

const pluginHook = makePluginHook([PgPubsub]);

const config = require(path.normalize(path.join(__dirname, "../../config/config.js")));

// require('pg-range').install(pg)

const DATABASE_HOST = config.DATABASE_HOST;
const DATABASE_PORT = config.DATABASE_PORT;
const DATABASE_USER = config.DATABASE_USER;
const DATABASE_PASSWORD = config.DATABASE_PASSWORD;
const DATABASE_NAME = config.DATABASE_NAME;
const GRAPHQL_HOST = config.GRAPHQL_HOST;
const GRAPHQL_PORT = config.GRAPHQL_PORT;
// const DATABASE_POOL_MIN = config.DATABASE_POOL_MIN;
// const DATABASE_POOL_MAX = config.DATABASE_POOL_MAX;
// const DATABASE_TIMEOUT = config.DATABASE_TIMEOUT;
// const DATABASE_DEBUG = config.DEBUG;
// const NODE_ENV = config.NODE_ENV;

const connection = {
  host: DATABASE_HOST,
  port: DATABASE_PORT,
  user: DATABASE_USER,
  password: DATABASE_PASSWORD,
  database: DATABASE_NAME,
};

const graphileBuildOptions = {
  pgSimplifyAllRows: true,
  pgSimplifyPatch: true,
  pgShortPk: true,
  pgSubscriptionPrefix: "graphql:",
  pgOmitListSuffix: false,
  connectionFilterAllowedOperators: [
    "isNull",
    "equalTo",
    "notEqualTo",
    "distinctFrom",
    "notDistinctFrom",
    "lessThan",
    "lessThanOrEqualTo",
    "greaterThan",
    "greaterThanOrEqualTo",
    "in",
    "notIn",
  ],
  // connectionFilterAllowedFieldTypes: ["String", "Int"],
  connectionFilterComputedColumns: true, // default: true
  connectionFilterLists: true, // default: true
  connectionFilterOperatorNames: {
    // equalTo: "eq",
    // notEqualTo: "neq"
  },
  connectionFilterRelations: true, // default: false
  connectionFilterSetofFunctions: true, // default: true
  connectionFilterLogicalOperators: true, // default: true
  connectionFilterAllowNullInput: true, // default: false
  connectionFilterAllowEmptyObjectInput: true, // default: false
};

const postgresConn = `postgres://${connection.user}:${connection.password}@${connection.host}:${connection.port}/${connection.database}`;

const options = {
  pluginHook,
  appendPlugins: [
    PgSimplifyInflectorPlugin,
    ConnectionFilterPlugin,
    PgOrderByRelatedPlugin,
    PgManyToManyPlugin,
    // PgOrderByMultiColumnIndexPlugin
  ],
  watchPg: true,
  ownerConnectionString: "",
  classicIds: false,
  graphileBuildOptions: graphileBuildOptions,
  externalUrlBase: "",
  graphqlRoute: "/graphql",
  graphiqlRoute: "/graphiql",
  graphiql: true, //!IS_PRODUCTION,
  enhanceGraphiql: true, //!IS_PRODUCTION
  enableCors: true,
  subscriptions: true,
  simpleSubscriptions: true,
  // live: true, //  greater strain on database
  websocketMiddlewares: [],
  dynamicJson: true,
  ignoreRBAC: true,
  ignoreIndexes: false,
  showErrorStack: true, //!IS_PRODUCTION
  extendedErrors: ["hint", "detail", "errcode"],
  // extendedErrors: ['severity', 'code', 'detail', 'hint', 'position', 'internalPosition', 'internalQuery', 'where', 'schema', 'table', 'column', 'dataType', 'constraint', 'file', 'line', 'routine'],
  writeCache: "./cache/cache.bin",
  // readCache: "./cache/cache.bin",
  queryCacheMaxSize: 1048576 * 100, // 100MB
  exportJsonSchemaPath: path.join(__dirname, "./cache/schema.json"),
  bodySizeLimit: 1048576 * 5, // 5MB
  disableQueryLog: true, //!IS_PRODUCTION
  // jwtSecret: "123",
  // jwtVerifyOptions: "",
  // jwtPgTypeIdentifier: "discipuli.jwt_token",
  // pgDefaultRole: "anonymous",
  // jwtRole: ""
};

//postgraphile(DATABASE_URL, SCHEMAS, {pluginHook, subscriptions: true})
const postgraphileMiddleware = postgraphile(process.env.DB_HOST || postgresConn, options);

app.use(postgraphileMiddleware);

enhanceHttpServerWithSubscriptions(rawHTTPServer, postgraphileMiddleware);
rawHTTPServer.listen(parseInt(process.env.PORT, 10) || 3000);

// app.listen(parseInt(GRAPHQL_PORT, 10) || 3000);

console.log(
  `GraphQL listening on http://${GRAPHQL_HOST}:${GRAPHQL_PORT || 3000}${options.graphqlRoute}`,
  "\n",
  `GraphQL websocket listening on ws://${GRAPHQL_HOST}:${GRAPHQL_PORT || 3000}${
    options.graphqlRoute
  }`,
  "\n",
  `PostGraphiQL GUI: http://${GRAPHQL_HOST}:${GRAPHQL_PORT || 3000}${options.graphiqlRoute}`
);
