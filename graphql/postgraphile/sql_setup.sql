-- function

create function app_public.graphql_subscription() returns trigger as $$
declare
  v_process_new bool = (TG_OP = 'INSERT' OR TG_OP = 'UPDATE');
  v_process_old bool = (TG_OP = 'UPDATE' OR TG_OP = 'DELETE');
  v_event text = TG_ARGV[0];
  v_topic_template text = TG_ARGV[1];
  v_attribute text = TG_ARGV[2];
  v_record record;
  v_sub text;
  v_topic text;
  v_i int = 0;
  v_last_topic text;
begin
  for v_i in 0..1 loop
    if (v_i = 0) and v_process_new is true then
      v_record = new;
    elsif (v_i = 1) and v_process_old is true then
      v_record = old;
    else
      continue;
    end if;
     if v_attribute is not null then
      execute 'select $1.' || quote_ident(v_attribute)
        using v_record
        into v_sub;
    end if;
    if v_sub is not null then
      v_topic = replace(v_topic_template, '$1', v_sub);
    else
      v_topic = v_topic_template;
    end if;
    if v_topic is distinct from v_last_topic then
      -- This if statement prevents us from triggering the same notification twice
      v_last_topic = v_topic;
      perform pg_notify(v_topic, json_build_object(
        'event', v_event,
        'subject', v_sub
      )::text);
    end if;
  end loop;
  return v_record;
end;
$$ language plpgsql volatile
set search_path
from current;

------------------------------------------------------------
 -- triggers

CREATE TRIGGER _500_gql_update AFTER
UPDATE ON app_public.users
FOR EACH ROW EXECUTE PROCEDURE app_public.graphql_subscription( 'userChanged', -- the "event" string, useful for the client to know what happened
 'graphql:user:$1', -- the "topic" the event will be published to, as a template
 'id' -- If specified, `$1` above will be replaced with NEW.id or OLD.id from the trigger.
 );


CREATE TRIGGER _500_gql_update_member AFTER
INSERT
OR
UPDATE
OR
DELETE ON app_public.organization_members
FOR EACH ROW EXECUTE PROCEDURE app_public.graphql_subscription('organizationsChanged', 'graphql:user:$1', 'member_id');

-- create triggers for many tables at ownerConnectionString
 DO $$
BEGIN

EXECUTE (
SELECT string_agg(
  'CREATE TRIGGER touch_users
   BEFORE UPDATE ON ' || quote_ident(t) || '
   FOR EACH ROW
   WHEN (OLD.modification_time IS NOT DISTINCT FROM NEW.modification_time)
   EXECUTE PROCEDURE touch_modification_time();'
 , E'\n')
FROM unnest('{users, company, foo, bar}'::text[]) t -- list your tables here
);

END
$$;

-- add trigger to all TABLES

SELECT string_agg( format( 'CREATE TRIGGER log_up BEFORE UPDATE ON %s ' 'FOR EACH ROW EXECUTE PROCEDURE trg_log_up();', c.oid::regclass ), E'\n')
FROM pg_namespace n
JOIN pg_class c ON c.relnamespace = n.oid
WHERE n.nspname = 'public';

-- AND c.relname ~~* '%tbl%' -- to filter tables by name
 ---------------------------------------------------------
 LISTEN "postgraphile:change";


select pg_notify( 'postgraphile:change', json_build_object( '__node__', json_build_array('column_types', 44) )::text);

-----------------------------------------

CREATE FUNCTION notify_trigger() RETURNS trigger AS $$

DECLARE

BEGIN
 -- TG_TABLE_NAME is the name of the table who's trigger called this function
 -- TG_OP is the operation that triggered this function: INSERT, UPDATE or DELETE.
 -- NEW is the record that fired the trigger
 -- NEW.id value of the column 'id'
 -- NEW.name value of the column 'name'
 execute 'NOTIFY ' || TG_TABLE_NAME || '_' || TG_OP;
 return new;
END;

$$ LANGUAGE plpgsql;

--------------------------

CREATE FUNCTION notify_trigger() RETURNS trigger AS $$
DECLARE
BEGIN
-- TG_TABLE_NAME is the name of the table who's trigger called this function
 -- TG_OP is the operation that triggered this function: INSERT, UPDATE or DELETE.
 -- NEW is the record that fired the trigger
 -- NEW.id value of the column 'id'
 -- NEW.name value of the column 'name'
  PERFORM pg_notify('watch_' || lower(NEW.name), TG_TABLE_NAME || ',id,' || NEW.id );
  RETURN new;
END;
$$ LANGUAGE plpgsql;

----------------------------------------------
 -- Trigger notification for messaging to PG Notify
-- https://gist.github.com/colophonemes/9701b906c5be572a40a84b08f4d2fa4e

CREATE FUNCTION notify_trigger() RETURNS trigger AS 
$trigger$
DECLARE
  rec RECORD;
  payload TEXT;
  column_name TEXT;
  column_value TEXT;
  payload_items JSONB;
BEGIN
  -- Set record row depending on operation
  CASE TG_OP
  WHEN 'INSERT', 'UPDATE' THEN
     rec := NEW;
  WHEN 'DELETE' THEN
     rec := OLD;
  ELSE
     RAISE EXCEPTION 'Unknown TG_OP: "%". Should not occur!', TG_OP;
  END CASE;

  -- Get required fields
  FOREACH column_name IN ARRAY TG_ARGV LOOP
    EXECUTE format('SELECT $1.%I::TEXT', column_name)
    INTO column_value
    USING rec;
    payload_items := coalesce(payload_items,'{}')::jsonb || json_build_object(column_name,column_value)::jsonb;
  END LOOP;

  new_record := row_to_json(NEW.*);

  -- Build the payload
  payload := json_build_object(
    'timestamp',CURRENT_TIMESTAMP,
    'operation',TG_OP,
    'schema',TG_TABLE_SCHEMA,
    'table',TG_TABLE_NAME,
    -- 'data',payload_items
    'data',new_record
  );

  -- Notify the channel
  PERFORM pg_notify('db_notifications', payload);

  RETURN rec;
END;
$trigger$ LANGUAGE plpgsql;

----------------------------------------

CREATE TRIGGER column_types_notify AFTER
INSERT
OR
UPDATE
OR
DELETE ON public.column_types
FOR EACH ROW EXECUTE PROCEDURE notify_trigger(
  'id', 
  'uuid', 
  'string', 
  'text', 
  'integer', 
  'float', 
  'decimal', 
  'boolean', 
  'date', 
  'time', 
  'datetime', 
  'timestamp', 
  'json', 
  'jsonb', 
  'enum', 
  'created_at', 
  'updated_at', 
  'valid_to', 
  'valid_from', 
  'date_start', 
  'date_end'
);

