CREATE TABLE my_schema.parent_table (
    id SERIAL PRIMARY KEY,
    name TEXT,
    description TEXT,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

COMMENT ON TABLE my_schema.parent_table IS
'Provide a description for your parent table.';

CREATE TABLE my_schema.child_table (
    id SERIAL PRIMARY KEY,
    name TEXT,
    description TEXT,
    row_owner TEXT,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    parent_table_id INTEGER NOT NULL REFERENCES my_schema.parent_table(id)
);

COMMENT ON TABLE my_schema.child_table IS
'Provide a description for your child table.';

-- enable row level security
ALTER TABLE my_schema.child_table ENABLE ROW LEVEL SECURITY;

-- only members of the [managers] role to access rows, 
-- and only rows of [my_schema.child_table]
CREATE POLICY own_records_manager_only_policy ON my_schema.child_table TO managers
    USING (row_owner = current_user);

-- If no role is specified, or the special user name PUBLIC is used, 
-- then the policy applies to all users on the system. 
-- To allow all users to access only their own row in a users table
CREATE POLICY own_records_policy ON my_schema.child_table
    USING (user_name = current_user);

-- To use a different policy for rows that are being added to the table 
-- compared to those rows that are visible, multiple policies can be 
-- combined. This pair of policies would allow all users to view 
-- all rows in the users table, but only modify their own
CREATE POLICY user_sel_policy ON my_schema.child_table
    FOR SELECT
    USING (true);
CREATE POLICY user_mod_policy ON my_schema.child_table
    USING (user_name = current_user);