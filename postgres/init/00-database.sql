\connect my_database;

CREATE SCHEMA my_schema; 
CREATE SCHEMA restricted_schema AUTHORIZATION user_name;

SET search_path TO public,my_schema,restricted_schema;

-- allow connecting to the database from addresses other than localhost
-- edit parameters in postgresql.conf
ALTER SYSTEM SET listen_addresses TO '*'
SELECT pg_reload_conf();

-- Constrain ordinary users to user-private schemas. 
-- The first "public" is the schema, 
-- the second "public" means "every user"
REVOKE CREATE UPDATE DELETE ON SCHEMA public FROM PUBLIC; 

ALTER ROLE user SET search_path = "$user";