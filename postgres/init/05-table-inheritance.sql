CREATE TABLE cities (
    name            text,
    population      float,
    altitude        int     -- in feet
);

CREATE TABLE capitals (
    state           char(2)
) INHERITS (cities);


-- select from cities and capitals
SELECT name, altitude
    FROM cities
    WHERE altitude > 500;
  -- sugar * for the above default
  SELECT name, altitude
    FROM cities*
    WHERE altitude > 500;

-- select only from cities
SELECT name, altitude
    FROM ONLY cities
    WHERE altitude > 500;

-- list which table the record comes from (relname column)
SELECT p.relname, c.name, c.altitude
    FROM cities c, pg_class p
    WHERE c.altitude > 500 AND c.tableoid = p.oid;