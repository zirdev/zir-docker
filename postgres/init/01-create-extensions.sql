

-- uuid
  CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- dblink
-- connect to another database
-- https://www.postgresql.org/docs/12/dblink.html
  CREATE EXTENSION IF NOT EXISTS dblink;

-- multicorn
-- https://railsware.com/blog/postgresql-most-useful-extensions/
-- https://multicorn.org/
  -- Multicorn allows you to access any data source 
  -- in your PostgreSQL database. (other databases, files, internet)
-
  -- CREATE EXTENSION IF NOT EXISTS multicorn;

  -- CREATE SERVER multicorn_imap FOREIGN DATA WRAPPER multicorn
  -- options (
  --   wrapper 'multicorn.imapfdw.ImapFdw'
  -- );

-- elasticsearch INDEX
  -- CREATE EXTENSION IF NOT EXISTS zombodb;

-- pgMemento (audit trail)