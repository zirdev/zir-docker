CREATE TABLE partitioned_table_range(patent_id BIGINT, date DATE) WITH (OIDS=FALSE);

-- Range Partitioning
 -- function to partition table and insert data

CREATE OR REPLACE FUNCTION create_partition_and_insert() RETURNS trigger AS $BODY$
        DECLARE
          partition_date TEXT;
          partition TEXT;
        BEGIN
          partition_date := to_char(NEW.date,'YYYY_MM_DD');
          partition := TG_RELNAME || '_' || partition_date;
          IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
            RAISE NOTICE 'A partition has been created %',partition;
            EXECUTE 'CREATE TABLE ' || partition || ' (check (date = ''' || NEW.date || ''')) INHERITS (' || TG_RELNAME || ');';
          END IF;
          EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || TG_RELNAME || ' ' || quote_literal(NEW) || ').* RETURNING patent_id;';
          RETURN NULL;
        END;
      $BODY$ LANGUAGE plpgsql VOLATILE COST 100;

-- trigger to move record to correct partition

CREATE TRIGGER partitioned_table_range_insert_trigger
BEFORE
INSERT ON partitioned_table_range
FOR EACH ROW EXECUTE PROCEDURE create_partition_and_insert();

-- display all table partitions

CREATE VIEW show_partitions AS
SELECT nmsp_parent.nspname AS parent_schema,
       parent.relname AS parent,
       nmsp_child.nspname AS child_schema,
       child.relname AS child
FROM pg_inherits
JOIN pg_class parent ON pg_inherits.inhparent = parent.oid
JOIN pg_class child ON pg_inherits.inhrelid = child.oid
JOIN pg_namespace nmsp_parent ON nmsp_parent.oid = parent.relnamespace
JOIN pg_namespace nmsp_child ON nmsp_child.oid = child.relnamespace
WHERE parent.relname='partitioned_table_range' ;

-- POSTGRES 10+

CREATE TABLE measurement (
    city_id         int not null,
    logdate         date not null,
    peaktemp        int,
    unitsales       int
) PARTITION BY RANGE (logdate);


CREATE TABLE fruits (
   record_id int not null,
   fruit TEXT
   ) PARTITION BY LIST (record_id);
-- POSTGRES 11+
 CREATE TABLE fruits_default PARTITION OF fruits DEFAULT;


-- List Partitioning
 -- The table is partitioned by
 -- explicitly listing which key values appear in each partition.

  CREATE TABLE partition_list
  (
    dept_no     number,   
    part_no     varchar2,
    country     varchar2(20),
    date        date,
    amount      number
  )
  PARTITION BY LIST(country)
  (
    PARTITION europe VALUES("FRANCE", "ITALY"),
    PARTITION asia VALUES("INDIA", "PAKISTAN"),
    PARTITION americas VALUES("US", "CANADA")
  );

 -- HASH partitioning
  CREATE TABLE partition_hash (
      client_id   INTEGER, 
      name        TEXT
    ) PARTITION BY HASH (client_id);


    CREATE TABLE partition_hash_0 PARTITION OF partition_hash
    FOR
    VALUES WITH (MODULUS 3, REMAINDER 0);


    CREATE TABLE partition_hash_1 PARTITION OF partition_hash
    FOR
    VALUES WITH (MODULUS 3, REMAINDER 1);


    CREATE TABLE partition_hash_2 PARTITION OF partition_hash
    FOR
    VALUES WITH (MODULUS 3, REMAINDER 2);


SET constraint_exclusion = on;

---------------------------------------------------------------
-- List Partitioning Syntax
-- Use the first form to create a list-partitioned table:

-- CREATE TABLE [ schema. ]table_name 
--  table_definition
--    PARTITION BY LIST(column)
--    [SUBPARTITION BY {RANGE|LIST|HASH} (column[, column ]...)]
--    (list_partition_definition[, list_partition_definition]...);
-- Where list_partition_definition is:
-- PARTITION [partition_name]
--   VALUES (value[, value]...)
--   [TABLESPACE tablespace_name]
--   [(subpartition, ...)]


-- Range Partitioning Syntax
-- Use the second form to create a range-partitioned table:

-- CREATE TABLE [ schema. ]table_name 
--  table_definition
--    PARTITION BY RANGE(column[, column ]...)
--    [SUBPARTITION BY {RANGE|LIST|HASH} (column[, column ]...)]
--    (range_partition_definition[, range_partition_definition]...);
-- Where range_partition_definition is:
-- PARTITION [partition_name]
--   VALUES LESS THAN (value[, value]...)
--   [TABLESPACE tablespace_name]
--   [(subpartition, ...)]


-- Hash Partitioning Syntax
-- Use the third form to create a hash-partitioned table:

-- CREATE TABLE [ schema. ]table_name
--  table_definition
--    PARTITION BY HASH(column[, column ]...)
--    [SUBPARTITION BY {RANGE|LIST|HASH} (column[, column ]...)]
--    (hash_partition_definition[, hash_partition_definition]...);
-- Where hash_partition_definition is:
-- [PARTITION partition_name]
--   [TABLESPACE tablespace_name]
--   [(subpartition, ...)]

-- Subpartitioning Syntax
-- subpartition may be one of the following:

-- {list_subpartition | range_subpartition | hash_subpartition}
-- where list_subpartition is:
-- SUBPARTITION [subpartition_name]
--   VALUES (value[, value]...)
--   [TABLESPACE tablespace_name]
-- where range_subpartition is:
-- SUBPARTITION [subpartition_name]
--   VALUES LESS THAN (value[, value]...)
--   [TABLESPACE tablespace_name]
-- where hash_subpartition is:
-- [SUBPARTITION subpartition_name]
--   [TABLESPACE tablespace_name]