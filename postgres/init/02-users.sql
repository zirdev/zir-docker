\connect my_database;

-- change password for default database user
ALTER USER postgres WITH ENCRYPTED PASSWORD 'mypostgres_pwd9';

-- create database user/role (user = sugar for role)
CREATE USER zirdev WITH SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN ENCRYPTED PASSWORD ENCRYPTED PASSWORD 'zirdev';
GRANT ALL PRIVILEGES ON DATABASE my_database TO zirdev;

CREATE ROLE mysuperuser2 WITH SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN ENCRYPTED PASSWORD 'mysuperpass2';
  -- group role typically does not have LOGIN and password
  CREATE ROLE superusers WITH SUPERUSER CREATEDB CREATEROLE INHERIT;
-- grant databae user admin privileges


-- definition of privilege groups
CREATE TABLE groups (group_id int PRIMARY KEY,
                     group_name text NOT NULL);

INSERT INTO groups VALUES
  (1, 'low'),
  (2, 'medium'),
  (5, 'high');

GRANT ALL ON groups TO zirdev;  -- zirdev is the administrator
GRANT ALL ON groups TO alice;  -- alice is the administrator
GRANT SELECT ON groups TO public; -- all users (public) can see records

-- definition of users' privilege levels
CREATE TABLE users (user_name text PRIMARY KEY,
                    group_id int NOT NULL REFERENCES groups);

INSERT INTO users VALUES
  ('postgres', 2),
  ('zirdev', 5),
  ('alice', 5),
  ('bob', 2),
  ('mallory', 1);

GRANT ALL ON users TO alice;
GRANT SELECT ON users TO public;

-- table holding the information to be protected
CREATE TABLE information (info text,
                          group_id int NOT NULL REFERENCES groups);

INSERT INTO information VALUES
  ('barely secret', 1),
  ('slightly secret', 2),
  ('very secret', 5);

ALTER TABLE information ENABLE ROW LEVEL SECURITY;

-- a row should be visible to/updatable by users whose security group_id is
-- greater than or equal to the row's group_id
CREATE POLICY fp_s ON information FOR SELECT
  USING (group_id <= (SELECT group_id FROM users WHERE user_name = current_user));
CREATE POLICY fp_u ON information FOR UPDATE
  USING (group_id <= (SELECT group_id FROM users WHERE user_name = current_user));

-- we rely only on Row Level Security to protect the information table
-- grant table rights to all users
GRANT ALL ON information TO public;