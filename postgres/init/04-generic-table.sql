-- DROP TYPE enum_values;

CREATE TYPE enum_values AS ENUM (
	'value1',
	'value2',
	'value3');

-- DROP TABLE public.column_types;

CREATE TABLE my_schema.column_types (
	id serial NOT NULL,
	uuid uuid NULL DEFAULT uuid_generate_v4(),
	string varchar(255) NULL,
	text text NULL,
	integer int4 NULL,
	float float4 NULL,
	decimal numeric(8,2) NULL,
  num_positive numeric CONSTRAINT positive_number CHECK (price > 0)
	boolean bool NULL,
	date date NULL,
	time time NULL,
	datetime timestamp NULL DEFAULT CURRENT_TIMESTAMP(6),
	timestamp timestamptz NULL DEFAULT CURRENT_TIMESTAMP,
	json json NULL,
	jsonb jsonb NULL,
	enum enum_values NULL DEFAULT 'value1'::enum_values,
	created_at timestamptz NULL DEFAULT CURRENT_TIMESTAMP,
	updated_at timestamptz NULL DEFAULT CURRENT_TIMESTAMP,
	valid_from timestamp NULL,
	valid_to timestamp NULL,
	date_start abstime NULL,
	date_end abstime NULL,
	CONSTRAINT id PRIMARY KEY (id),
  CHECK (valid_to > valid_from AND id > 0),
  UNIQUE (id, uuid)
  -- FOREIGN KEY (b, c) REFERENCES other_table (b1, c1)
	--CONSTRAINT column_types_user_fk FOREIGN KEY ("integer") REFERENCES "user"(id)
);

CREATE INDEX id_timestamp_index ON public.column_types USING btree (id, "timestamp");
CREATE INDEX integer_index ON public.column_types USING btree ("integer");
CREATE INDEX idx1 ON public.column_types USING btree (id) INCLUDE (id, uuid);

--